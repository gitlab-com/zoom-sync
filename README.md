# README

This script uploads recordings from our Zoom account and uploads them to
Google Drive. We originally did this because the Zoom storage was
limited and costly, but we continue do this to enable all team members to
view the videos.

NOTE: The script will **only** copy meetings that have `[REC]` in the
meeting title. However, `zoom_sync.yml` can be configured with an [allow list](#allow-list).

NOTE: This script no longer purges videos from Zoom, and it does not
retain state. Because of this, the script will scan yesterday's videos and
only upload them if they do not exist on Google Drive already. If the
script does not run in a day, it will currently skip over those videos.

## Configuration

`zoom-sync.rb` can be run solely via environment variables. 

### Environment variables

|Variable|Description|
|--------|-----------|
|ZOOM_ACCOUNT_ID|Zoom Account ID|
|ZOOM_CLIENT_ID|Zoom Application Client ID|
|ZOOM_CLIENT_SECRET|Zoom Application Client Secret|
|GOOGLE_DRIVE_SERVICE_ACCOUNT_KEY|Google Drive credentials|
|GOOGLE_DRIVE_ID|Google Drive ID for destination folder (e.g. `0A1PeuBAntm4LNk9PVB`)|

Environment variables take precedence over the `zoom_sync.yml` values.

If `GOOGLE_DRIVE_SERVICE_ACCOUNT_KEY` is not available, the script will
attempt to load `config.json`. If that file does not exist, the Google
API will attempt to obtain to retrieve the credentials from your
existing Google Cloud account. An example JSON:

```json
{
  "type": "service_account",
  "project_id": "<your project id",
  "private_key_id": "<your private key id>",
  "private_key": "-----BEGIN PRIVATE KEY-----\nREDACTED-----END PRIVATE KEY-----\n",
  "client_email": "<your client email>",
  "client_id": "<your client id>",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "<your cclient_x509_cert_url>"
}
```

## Allow List

`zoom_sync.yml` can also specify a list of recordings that match a given
e-mail address and topic:

```yaml
---
allow_list:
  - email: someone@gitlab.com
    regex: Group Conversation
  - email: someone@gitlab.com
    regex: Important Topic
```

## Run-time parameters

If you need to sync a meeting that was created previous to the current
date, you can also specify a date range (up to 6 months from today):

|Variable|Description|
|--------|-----------|
|START_DATE|Starting date to search for recordings (YYYY-MM-DD)|
|END_DATE|End date to search for recordings (YYYY-MM-DD)|
|EMAIL|By default, the script will search all accounts for videos. This variable restrict scan for recordings to specific e-mail addresses.|